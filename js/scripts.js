class VideoPlayer {
    constructor() {
        this.player = document.querySelector('.player');
        this.video = this.player.querySelector('.viewer');
        this.progress = document.querySelector('.progress');
        this.progressBar = this.progress.querySelector('.progress__filled');
        this.toggle = this.player.querySelector('.toggle');
        this.skipButton = this.player.querySelectorAll('[data-skip]');
        this.ranges = this.player.querySelectorAll('.player__slider');
        this.properties = JSON.parse(localStorage.getItem('video')) || {
            currentTime: 0, playbackRate: 1, volume: 1
        };
    }

    init() {
        // Start plugin
        this.progressBar.style.flexBasis = '0';
        this.events();
        this.getProp();
    }

    events() {
        // All events
        this.video.addEventListener('click', e => this.togglePlay(e));
        this.toggle.addEventListener('click', e => this.togglePlay(e));
        this.ranges.forEach(range => range.addEventListener('change', e => this.handleRangeUpdate(e)));
        this.ranges.forEach(range => range.addEventListener('mousemove', e => this.handleRangeUpdate(e)));
        this.skipButton.forEach(btn => btn.addEventListener('click', e => this.skip(e)));
        this.video.addEventListener('timeupdate', e => {this.updateProgressBar(e); this.saveProp()});
        this.progress.addEventListener('click', e => this.chooseMoment(e));
        this.progress.addEventListener('mousemove', e => this.mouseDown && this.chooseMoment(e));
        this.progress.addEventListener('mousedown', () => this.mouseDown = true);
        this.progress.addEventListener('mouseup', () => this.mouseDown = false);
        this.video.addEventListener('ended', e => this.stopPlayer(e));
    }

    togglePlay() {
        // Play/Pause video
        const method = this.video.paused ? 'play' : 'pause';

        this.toggle.textContent = this.video.paused ? '❚ ❚' : '►';
        this.video[method]();
    }

    handleRangeUpdate(e) {
        this.video[e.target.name] = e.target.value;
    }

    skip(e) {
        // Time skip
        this.video.currentTime += parseFloat(e.target.dataset.skip);
    }

    updateProgressBar() {
        let percentage = Math.floor((100 / this.video.duration) * this.video.currentTime);

        this.progressBar.style.flexBasis = percentage + '%';
    }

    chooseMoment(e) {
        this.video.currentTime = this.video.duration * e.offsetX / this.progress.offsetWidth;
    }

    saveProp() {
        this.properties.currentTime = this.video.currentTime;
        this.properties.playbackRate = this.video.playbackRate;
        this.properties.volume = this.video.volume;

        let serialProp = JSON.stringify(this.properties);
        localStorage.setItem('video', serialProp);
    }

    getProp() {
        this.video.currentTime = this.properties.currentTime;
        this.video.playbackRate = this.properties.playbackRate;
        this.video.volume = this.properties.volume;
        this.ranges.forEach(range => range.value = this.properties[range.name]);
    }

    stopPlayer() {
        this.toggle.textContent = '►';
        this.video.pause();
        this.video.currentTime = 0;
    }
}

const video = new VideoPlayer();

video.init();