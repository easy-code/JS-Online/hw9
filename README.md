##### Cсылка на видео
[Video](https://drive.google.com/drive/folders/1LFEZblqzqgHn01zaPH_hdyhEPXDlAU3g)

##### Ссылки на презентацию
[First](https://docs.google.com/presentation/d/1Zm2Cbr1nH4Z_nBA6cuoME2cAa4xZ3t59HkPzblMyMI8/pub?start=false&loop=false&delayms=3000&slide=id.p)

[Second](https://docs.google.com/presentation/d/1PsExVlwf3CXzKy6oMd_cDKdxftXVZm6rUoJCOoYDWYs/pub?start=false&loop=false&delayms=3000&slide=id.p)

##### Справочники
[developer.mozilla.org](https://developer.mozilla.org/uk/docs/Web/JavaScript)

[learn.javascript.ru](https://learn.javascript.ru/getting-started)

##### Стиль кода
https://learn.javascript.ru/coding-style
